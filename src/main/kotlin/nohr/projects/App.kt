package nohr.projects

import java.io.BufferedReader
import java.io.InputStreamReader

import nohr.projects.api.GitLabAPI
import nohr.projects.data.Group
import nohr.projects.data.Project

fun main(args: Array<String>) {
    val allGroups = getAllGroups()

    //hold the full list of projects
    val allProjects = mutableListOf<Project>()
    
    allGroups.forEach { nextGroup ->
        println("${nextGroup.code}   ${nextGroup.fullPath} [${nextGroup.name}]")
        nextGroup.projects?.forEach { nextProject ->
            println("   ${nextProject.code}   ${nextProject.name}")
            allProjects.add(nextProject)
        }
    }

    var keepGoing = true
    
    while(keepGoing) {
        println("What project do you want to move?")
        val projectCode = readString()
        val projectToMove = allProjects.find{ it.code.equals(projectCode, true) }
        //println("You entered: $projectToMove")

        println("Where do you want to move it?")
        val groupCode = readString()
        val groupDestination = allGroups.find{ it.code.equals(groupCode, true) }
        //println("You entered: $groupDestination")

        if(projectToMove == null || groupDestination == null) {
            keepGoing = false
        } else {
            GitLabAPI().moveProjectToGroup(projectToMove!!, groupDestination!!)
        }
    }
}

fun getAllGroups(): List<Group> {
    println("Reading from the API...")
    val api = GitLabAPI()

    //first get the base list of user projects
    val userGroup: Group = api.getUserMainGroup()
    userGroup.projects = api.listProjectsForUser()

    //then get all the sub groups with projects
    val groups: List<Group> = api.listGroups()
    groups.forEach{ nextGroup->
        nextGroup.projects = api.listProjectsByGroup(nextGroup)
    }

    //combine the lists
    val allGroups = mutableListOf<Group>()
    allGroups.add(userGroup)
    allGroups.addAll(groups)

    //add the codes
    var groupCounter = 'A'
    var projectCounter = 1
    allGroups.forEach { nextGroup ->
        nextGroup.code = groupCounter.toString()
        
        nextGroup.projects?.forEach { nextProject ->
            nextProject.code = "${groupCounter}${projectCounter}"            
            projectCounter++
        }

        groupCounter++
        projectCounter = 1
    }

    return allGroups
}

fun readString(): String {
    val input = BufferedReader(InputStreamReader(System.`in`))
    
    val enteredCode = input.readLine()
    
    if("" != enteredCode.trim()) {
        return enteredCode.trim()
    } else {
        println("ERROR: You did not enter anything")
        System.exit(-1)
    }

    //this shouldn't happen since I'll either return a valid code or exit
    return ""
}