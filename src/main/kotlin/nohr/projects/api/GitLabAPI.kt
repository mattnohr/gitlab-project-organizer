package nohr.projects.api

import java.net.URLEncoder
 
import khttp.get
import khttp.put

import nohr.projects.LocalProperty
import nohr.projects.LocalProperties

import nohr.projects.data.Group
import nohr.projects.data.Project

class GitLabAPI {
    val authToken: String
    val userId: String

    init {
        authToken = LocalProperties().get(LocalProperty.GITLAB_AUTH) ?: "not found"
        userId = LocalProperties().get(LocalProperty.USER_ID) ?: "not found"
    }

    fun getUserMainGroup(): Group {
        val r = get("https://gitlab.com/api/v4/users/$userId", headers = mapOf("Private-Token" to authToken))
        val resultObject = r.jsonObject
        val path = resultObject.getString("username")
        val webURL = resultObject.getString("web_url")
        
        return Group(0, "Main", path, webURL)
    }

    fun listGroups(): List<Group> {
        val r = get("https://gitlab.com/api/v4/groups/", headers = mapOf("Private-Token" to authToken))
        val resultArray = r.jsonArray
        val groups = mutableListOf<Group>()
        for (i in 0..(resultArray.length() - 1)) {
            val nextGroupJSON = resultArray.getJSONObject(i)
            val nextGroup = Group(nextGroupJSON.getInt("id"), nextGroupJSON.getString("name"), nextGroupJSON.getString("path"), nextGroupJSON.getString("full_path"))
            groups.add(nextGroup)
        }

        groups.sortBy{ it.fullPath }
        
        return groups
    }

    fun listProjectsForUser(): List<Project> {
        val r = get("https://gitlab.com/api/v4/users/${userId}/projects", headers = mapOf("Private-Token" to authToken))
        val resultArray = r.jsonArray
        val projects = mutableListOf<Project>()
        for (i in 0..(resultArray.length() - 1)) {
            val nextProjectJSON = resultArray.getJSONObject(i)
            val nextProject = Project(nextProjectJSON.getInt("id"), nextProjectJSON.getString("name"))
            projects.add(nextProject)
        }

        projects.sortBy{ it.name }
        
        return projects
    }

    fun listProjectsByGroup(group: Group): List<Project> {
        val r = get("https://gitlab.com/api/v4/groups/${group.id}/projects", headers = mapOf("Private-Token" to authToken))
        val resultArray = r.jsonArray
        val projects = mutableListOf<Project>()
        for (i in 0..(resultArray.length() - 1)) {
            val nextProjectJSON = resultArray.getJSONObject(i)
            val nextProject = Project(nextProjectJSON.getInt("id"), nextProjectJSON.getString("name"))
            projects.add(nextProject)
        }

        projects.sortBy{ it.name }
        
        return projects
    }

    fun moveProjectToGroup(project: Project, group: Group) {
        println("Moving ${project.name} to ${group.name}")
        val groupId = if (group.id == 0) group.path else group.id.toString()
        val encodedGroupId = URLEncoder.encode(groupId, "utf-8")


        println("DEBUG: Calling https://gitlab.com/api/v4/projects/${project.id}/transfer with namespace ${encodedGroupId}")

        val r = put("https://gitlab.com/api/v4/projects/${project.id}/transfer", 
            headers = mapOf("Private-Token" to authToken),
            params = mapOf("namespace" to encodedGroupId))

        if(r.statusCode != 200) {
            println("Status = ${r.statusCode} - Result = ${r.text}")
        }
    }
}