package nohr.projects.data

data class Group(val id: Int, val name: String, val path: String, val fullPath: String, var projects: List<Project>? = null, var code: String? = null)