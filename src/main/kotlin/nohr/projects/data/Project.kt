package nohr.projects.data

data class Project(val id: Int, val name: String, var code: String? = null)