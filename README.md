
## Usage

This rough first pass provides a list of all projects and what group they are in. 

To move a project to a new group, you enter the code of the project, like `B2`, and then enter the code of the group to move it to, like `C`.

## Details

Projects can be under the main user's namespace or in subgroups. There are different APIs to get these so when getting the list of groups and projects you need to actually call a few different APIs to gather all the data.

## Setup

Copy the `local.properties.sample` file to `local.properties` and update the values.

## Run

`./gradlew run`

## APIs

* List Groups: https://docs.gitlab.com/ee/api/groups.html#list-groups
* List Projects for Group: https://docs.gitlab.com/ee/api/groups.html#list-a-groups-projects
* Get User group (base URL): https://docs.gitlab.com/ee/api/users.html#single-user
* List projects for user group (not in sub group): https://docs.gitlab.com/ee/api/projects.html#list-user-projects
* Transfer Project: https://docs.gitlab.com/ee/api/projects.html#transfer-a-project-to-a-new-namespace